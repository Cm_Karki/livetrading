<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('live-share-price', 'Api\ApiLiveTodayPriceController@index');
Route::get('live-nepse-index', 'Api\ApiLiveTodayPriceController@nepseIndex');
Route::get('live-nepse', 'Api\ApiLiveTodayPriceController@nepse');
Route::get('live-data', 'Api\ApiLiveTodayPriceController@liveData');
Route::get('gainer-losser', 'Api\ApiLiveTodayPriceController@gainerlosser');
Route::get('today-nepse', 'Api\ApiLiveTodayPriceController@todayNepse');
Route::get('market-statistics', 'Api\ApiLiveTodayPriceController@marketStat');
Route::get('stock-volume','Api\ApiLiveTodayPriceController@stockVolume');
Route::get('/floorsheet/live-trade-analysis', 'Api\ApiFloorsheetController@liveTradeAnalysis');
Route::get('/floorsheet/live-broker-analysis', 'Api\ApiFloorsheetController@liveBrokerAnalysis');

Route::get('/today-traded-stocks', 'Api\ApiLiveDataController@todayTradedStocks');
Route::get('/today-traded-indices', 'Api\ApiLiveDataController@todayTradedIndices');
Route::get('/today-floorsheet', 'Api\ApiLiveDataController@todayFloorsheet');

Route::get('/company-live-price', 'Api\ApiCompanyController@livePrice');
Route::get('/latest-stock-trading-date', 'Api\ApiCompanyController@latestStockTradingDate');
Route::get('/company-live-price-analysis', 'Api\ApiCompanyController@livePriceAnalysis');
Route::get('/company-live-price-analysis/sectorwise-gain', 'Api\ApiCompanyController@liveSectorwiseGain');
