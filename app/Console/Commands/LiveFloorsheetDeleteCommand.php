<?php
/**
 * Created by PhpStorm.
 * User: rajendra
 * Date: 5/30/18
 * Time: 12:10 PM
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;


class LiveFloorsheetDeleteCommand extends Command
{
    public $baseUrl = "http://www.nepalstock.com";

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:floorsheetdelete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get live Floorsheet Delete';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function handle()
    {
        
                    DB::table('tbl_live_floorsheet')->delete();
            
    }

    
}