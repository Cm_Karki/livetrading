<?php

namespace App\Console\Commands\api;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: rajendra
 * Date: 1/10/19
 * Time: 11:25 AM
 */
class IndexCommand extends Command
{
    //public $baseUrl = "http://http://118.91.175.170";
    public $baseUrl = "http://nepse.co/Modules/Admin/ShareApi/ShareApi.asmx/GetMarketInfo?Date=";

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get live stock Index';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function handle()
    {
        $this->getData();
    }

    public function getData()
    {
        \Log::info("Get data at live index " . date("Y-m-d h:i:s"));
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $this->baseUrl)->getBody();
        $xml = simplexml_load_string($res->getContents());
        $json = json_encode($xml);
        $results = json_decode($json,TRUE);
        $datas = array();
        foreach ($results['IndexInfoAPI'] as $key => $value){
            $total = DB::table('tbl_live_index')->where('created_at', '=', $value['Date'])
                ->where('_index', '=', $value['IndexName'])->count();
            if(!($total > 0)){
                $data = array();
                $data['_index'] = $value['IndexName'];
                $data['turnover'] = $value['StatTurnOver'];
                $data['current'] = $value['IndexValue'];
                $data['change_'] = $value['AbsoluteChange'];
                $data['per_change'] = $value['PercentageChange'];
                $data['inc_dec'] = $value['PercentageChange'] < 0 ? -1 : ($value['PercentageChange'] > 0 ? 1 : 0);
                $data['created_at'] = $value['Date'];
                array_push($datas, $data);
            }
        }
        try{

        }catch(\Exception $e){

        }
        if (count($datas) > 0) {
            DB::table('tbl_live_index')->insert($datas);
        }
    }
}