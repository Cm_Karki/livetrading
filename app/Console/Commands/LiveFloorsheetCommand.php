<?php
/**
 * Created by PhpStorm.
 * User: rajendra
 * Date: 5/30/18
 * Time: 12:10 PM
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;


class LiveFloorsheetCommand extends Command
{
    public $baseUrl = "http://www.nepalstock.com";

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:floorsheet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get live Floorsheet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function handle()
    {
        \Log::info("Get data at live floorsheet " . date("Y-m-d h:i:s"));
        $url = $this->baseUrl . "/main/floorsheet/index/?contract-no=&stock-symbol=&buyer=&seller=&_limit=30000";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36');
        $results=curl_exec($curl);
        $crawler = new Crawler($results);
        $datas = array();
        $table = $crawler->filter(".table");
        $table->filter("tr")->each(function ($tr) use(&$datas,&$total){
            $tr->filter('td')->each(function ($td)
            use (&$array) {
                $array[] = trim($td->text());
            });
            $strTodayDate = date('Ymd');
            if(array_key_exists(2, $array)){
                if( strpos( $array[1], $strTodayDate ) !== false){
                    $total = DB::table('tbl_live_floorsheet')->where('contract_no','=', $array[1])->count();
                    if(!($total > 0)){
                        $data = array();
                        $data['contract_no'] = $array[1];
                        $data['symbol'] = $array[2];
                        $data['buyer'] = $array[3];
                        $data['seller'] = $array[4];
                        $data['quantity'] = (float)$array[5];
                        $data['rate'] = (float)$array[6];
                        $data['amount'] = (float)$array[7];
                        array_push($datas, $data);
                    }
                }
            }

        });
        if (count($datas) > 0) {
            DB::table('tbl_live_floorsheet')->insert($datas);
        }

    }


}