<?php
/**
 * Created by PhpStorm.
 * User: rajendra
 * Date: 5/30/18
 * Time: 12:10 PM
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;


class LiveStockCommand extends Command
{
    public $baseUrl = "http://www.nepalstock.com";

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:live';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get live stock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function handle()
    {
        \Log::info("Get data at live stock " . date("Y-m-d h:i:s"));
        // Log::info("Scheduling At " . date('Y-m-d H:i:s'));
        $url = $this->baseUrl . "/stocklive";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36');
        $results=curl_exec($curl);
        $crawler = new Crawler($results);
        $date = "";
        $time = "";
        $dateDiv = $crawler->filter("#ticker > #date");
        $dateString = trim($dateDiv->text());
        $splits = explode(" ", $dateString);
        if (count($splits) == 7) {
            $date = $splits[2];
            $time = $splits[5];
        }
        Log::info("Incomming Date " . $date . " " . $time);
        if (!empty($date) && !empty($time)) {
            $dateTime = $date . ' ' . $time;
            $total = DB::table('tbl_live_stock')->where('created_at', '=', $dateTime)->count();
            if (!($total > 0)) {
                $datas = array();
                $table = $crawler->filter("div.col-xs-12.col-md-9.col-sm-9 > table");
                $table->filter("tbody > tr")->each(function ($tr) use(&$dateTime, &$datas){
                    $tr->filter('td')->each(function ($td)
                    use (&$array) {
                        $array[] = trim($td->text());
                    });
                    if(str_replace(',', '', $array[3]) >= 10){
                        $data = array();
                        $data['symbol'] = $array[1];
                        $data['ltp'] = str_replace(',', '', $array[2]);
                        $data['ltv'] = str_replace(',', '', $array[3]);
                        $data['change_'] = $array[4];
                        $data['per_change'] = $array[5];
                        $data['open'] = str_replace(',', '', $array[6]);
                        $data['high'] = str_replace(',', '', $array[7]);
                        $data['low'] = str_replace(',', '', $array[8]);
                        $data['traded_quantity'] = str_replace(',', '', $array[9]);
                        $data['prev_close'] = str_replace(',', '', $array[10]);
                        $data['created_at'] = $dateTime;
                        array_push($datas, $data);
                    }
                });
                $turnovers = array();
                $table = $crawler->filter("#top-by-turnover");
                $table->filter("tbody > tr")->each(function ($tr) use(&$dateTime, &$turnovers){
                    $tr->filter('td')->each(function ($td)
                    use (&$array) {
                        $array[] = trim($td->text());
                    });
                        if (count($array) > 2) {
                            $turnover = array();
                            $turnover['symbol'] = $array[0];
                            $turnover['turnover'] = str_replace(',', '', $array[1]);
                            $turnover['ltp'] = str_replace(',', '', $array[2]);
                            $turnover['created_at'] = $dateTime;
                            array_push($turnovers, $turnover);
                        }
                });
                if (count($turnovers) > 0) {
                    DB::table('tbl_live_turnover')->insert($turnovers);
                }
                
                if (count($datas) > 0) {
                    DB::table('tbl_live_stock')->insert($datas);
                }
            }
            Log::info("Finished scheduling");
        }
    }
}