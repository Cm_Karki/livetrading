<?php
/**
 * Created by PhpStorm.
 * User: rajendra
 * Date: 5/30/18
 * Time: 12:10 PM
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;


class LiveNewTurnoverCommand extends Command
{
    public $baseUrl = "http://www.nepalstock.com";

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:newturnover';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get live stock Total After 3 PM Turnover';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function handle()
    {
        $url = $this->baseUrl;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36');
        $results=curl_exec($curl);
        $crawler = new Crawler($results);
        $date = "";
        $time = "";
        $dateDiv = $crawler->filter("#ticker > #date");
        $dateString = trim($dateDiv->text());
        $splits = explode(" ", $dateString);
        if (count($splits) == 7) {
            $date = $splits[2];
            $time = $splits[5];
        }
        if (!empty($date) && !empty($time)) {
            $dateTime = $date . ' ' . $time;
            $newDate = date("Y-m-d") . ' ' . '15:00:00';
            $total = DB::table('tbl_live_market_summary')->where('created_at', '=', $newDate)->count();
            if (($total > 0)) {
                DB::table('tbl_live_market_summary')->where('created_at', '=', $newDate)->delete();
                Log::info("New Turnover index updated");
                $datas = array();
                $table = $crawler->filter("#nepse-stats > div:nth-child(2) > .panel-body > table");
               
                $table->filter("tbody > tr")->each(function ($tr) use(&$dateTime, &$datas){
                    $tr->filter('td')->each(function ($td)
                    use (&$array) {
                        $array[] = trim($td->text());
                       
                    });
                    
                    
                    if (count($array) >= 2) {
                        $data = array();
                        $data['title'] = $array[0];
                        $data['detail'] = $array[1];
                        $data['created_at'] = $dateTime;
                        array_push($datas, $data);
                    }
                });
             
                if (count($datas) > 0) {
                    DB::table('tbl_live_market_summary')->insert($datas);
                }
                
            }
            
        }
    }
}