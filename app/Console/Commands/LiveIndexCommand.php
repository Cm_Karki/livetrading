<?php
/**
 * Created by PhpStorm.
 * User: rajendra
 * Date: 5/30/18
 * Time: 12:10 PM
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;


class LiveIndexCommand extends Command
{
    //public $baseUrl = "http://http://118.91.175.170";
    public $baseUrl = "http://www.nepalstock.com";

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get live stock Index';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function handle()
    {
        $this->getData();
    }

    public function getData(){
        \Log::info("Get data at live index " . date("Y-m-d h:i:s"));
        $url = $this->baseUrl;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36');
        $results=curl_exec($curl);
        //\Log::info($results);
        $crawler = new Crawler($results);
        $date = "";
        $time = "";
        $dateDiv = $crawler->filter("#ticker > #date");
        $dateString = trim($dateDiv->text());
        $splits = explode(" ", $dateString);
        if (count($splits) == 7) {
            $date = $splits[2];
            $time = $splits[5];
        }
        if (!empty($date) && !empty($time)) {
            $dateTime = $date . ' ' . $time;
            $total = DB::table('tbl_live_index')->where('created_at', '=', $dateTime)->count();
            if (!($total > 0)) {
                $datas = array();
                $table = $crawler->filter("#nepse-stats > div:nth-child(3) > .panel-body > table:nth-child(1)");
                $html = $table->html();
                $html = str_replace("<!-- ", "", $html);
                $html = str_replace(" -->", "", $html);
                $index = new Crawler($html);
                $index->filter("tbody > tr")->each(function ($tr) use(&$crawler, &$dateTime, &$datas){
                    $tr->filter('td')->each(function ($td)
                    use (&$array) {
                        $tdText = $td->html();
                        if(strpos($tdText, 'increase.gif') !== false){
                            $array[] = 1;
                        } else  if(strpos($tdText, 'decrease.gif') !== false){
                            $array[] = -1;
                        }else  if(strpos($tdText, 'nil.gif') !== false){
                            $array[] = 0;
                        }
                        else{
                            $array[] = trim($td->text());
                        }
                       
                    });
                    
                    
                    if (count($array) > 2) {
                        $data = array();
                        $name = $array[0];
                        $turnover = str_replace(',', '', $array[1]);
                        if($name == "NEPSE"){
                            $turnover = $crawler->filter("#nepse-stats > div:nth-child(2) > .panel-body > table > tbody > tr:nth-child(1) > td:nth-child(2)")->text();
                            $turnover = str_replace(',', '', $turnover);
                        }
                        $data['_index'] = $name;
                        $data['turnover'] = $turnover;
                        $data['current'] = str_replace(',', '', $array[2]);
                        $data['change_'] = $array[3];
                        $data['per_change'] = (float)$array[4];
                        $data['inc_dec'] = $array[5];
                        $data['created_at'] = $dateTime;
                        array_push($datas, $data);
                    }
                });
                $sub_indices = array();
                $table = $crawler->filter("#nepse-stats > div:nth-child(3) > .panel-body > table:nth-child(2)");
                $html = $table->html();
                $html = str_replace("<!-- ", "", $html);
                $html = str_replace(" -->", "", $html);
                $subIndex = new Crawler($html);
                $subIndex->filter("tbody > tr")->each(function ($tr) use(&$dateTime, &$sub_indices){
                    $tr->filter('td')->each(function ($td)
                    use (&$array) {
                        $tdText = $td->html();
                        if(strpos($tdText, 'increase.gif') !== false){
                            $array[] = 1;
                        } else  if(strpos($tdText, 'decrease.gif') !== false){
                            $array[] = -1;
                        }else  if(strpos($tdText, 'nil.gif') !== false){
                            $array[] = 0;
                        }else{
                            $array[] = trim($td->text());
                        }
                        
                    });
                        if (count($array) > 2) {
                            $sub_index = array();
                            $sub_index['_index'] = $array[0];
                            $sub_index['turnover'] = str_replace(',', '', $array[1]);
                            $sub_index['current'] = str_replace(',', '', $array[2]);
                            $sub_index['change_'] = $array[3];
                            $sub_index['per_change'] = (float)$array[4];
                            $sub_index['inc_dec'] = $array[5];
                            $sub_index['created_at'] = $dateTime;
                            array_push($sub_indices, $sub_index);
                        }
                });

                if (count($datas) > 0) {
                    DB::table('tbl_live_index')->insert($datas);
                }
                if (count($sub_indices) > 0) {
                    DB::table('tbl_live_index')->insert($sub_indices);
                }
                
            }
            
        }
    }
}