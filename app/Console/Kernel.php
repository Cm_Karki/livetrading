<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\LiveStockCommand',
        'App\Console\Commands\LiveIndexCommand',
        'App\Console\Commands\LiveTurnoverCommand',
        'App\Console\Commands\LiveNewTurnoverCommand',
        'App\Console\Commands\LiveFloorsheetCommand',
        'App\Console\Commands\LiveFloorsheetDeleteCommand'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('stock:index')
            ->everyMinute()
            ->between("11:00", "14:59");

        $schedule->command('stock:turnover')
            ->everyMinute()
            ->between("11:00", "14:59");

        $schedule->command('stock:live')
            ->everyMinute()
            ->between("11:00", "15:00");

        $schedule->command('stock:floorsheet')
            ->cron('*/2 11-15 * * *');

        $schedule->command('stock:index')
            ->dailyAt("15:00");

        $schedule->command('stock:newturnover')
            ->dailyAt("15:00");

        $schedule->command('stock:floorsheet')
            ->dailyAt("15:01");

        $schedule->command('stock:floorsheetdelete')
            ->dailyAt("22:00"); 
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
