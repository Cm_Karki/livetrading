<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ApiCompanyController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function livePrice(Request $request)
    {
        if (!$request->has('symbol'))
            return response()->json([
                'message' => 'Bad request.',
                'type' => 'Error'
            ], 404);
        $price = array();
        $result = DB::table('tbl_live_stock')
            ->select(DB::raw("ltp as close, created_at as published_date"))
            ->where('symbol', '=', $request->input('symbol'))
            ->orderBy('created_at', 'DESC')
            ->first();
        if (isset($result)) {
            $price['close'] = $result->close;
            $price['published_date'] = $result->published_date;
        }

        return response()->json([
            'message' => 'Search result(s) found.',
            'type' => 'Success',
            'price' => $price
        ], 200);
    }

    public function latestStockTradingDate(Request $request)
    {
        $livePrice = array();
        $result = DB::table('tbl_live_stock')
            ->select(DB::raw("MAX(created_at) as published_date"))
            ->first();
        if (isset($result))
            $livePrice['published_date'] = $result->published_date;

        return response()->json([
            'message' => 'Search result(s) found.',
            'type' => 'Success',
            'livePrice' => $livePrice
        ], 200);
    }

    public function livePriceAnalysis(Request $request)
    {
        if (!$request->has('symbol'))
            return response()->json([
                'message' => 'Bad request.',
                'type' => 'Error'
            ], 404);
        $lstDate = DB::table('tbl_live_index')
            ->select(DB::raw("MAX(created_at) as date"))
            ->first();
        $date = date('Y-m-d');
        if (isset($lstDate))
            $date = date('Y-m-d', strtotime($lstDate->date));

        $results = DB::table('tbl_live_stock')
            ->select(DB::raw('ltp, ltv'))
            ->where('symbol', '=', $request->input('symbol'))
            ->where('created_at', '>=', $date)
            ->get();
        $total = 0;
        $totalVlmn = 0;
        foreach ($results as $result) {
            $total = $total + $result->ltp*$result->ltv;
            $totalVlmn = $totalVlmn + $result->ltv;
        }
        $vwap = $totalVlmn > 0 ? $total/$totalVlmn : 0.00;
        return response()->json([
            'message' => 'Search result(s) found.',
            'type' => 'Success',
            'price' => $vwap
        ], 200);
    }

    public function liveSectorwiseGain(Request $request)
    {
        $datas = $request->all();
        $companies = array();
        $sectorName = array();
        foreach ($datas as $data) {
            array_push($sectorName, $data['sectorname']);
            array_push($companies, $data['symbol']);
        }
        $results = DB::table("tbl_live_stock")
            ->select(DB::raw('COALESCE(SUBSTRING_INDEX(GROUP_CONCAT(CAST(ltp AS CHAR) ORDER BY created_at DESC),",",1), 0) as ltp, symbol, 
                    COALESCE(SUBSTRING_INDEX(GROUP_CONCAT(CAST(created_at AS CHAR) ORDER BY created_at DESC),",",1), 0) as created_at'))
            ->whereIn("symbol", $companies)
            ->orderBy('symbol', "ASC")
            ->orderBy('created_at', "DESC")
            ->groupBy('symbol')
            ->get();

        $price = array();
        foreach ($results as $key => $result) {
            $company['ltp'] = $result->ltp;
            $company['symbol'] = $result->symbol;
            $company['published_date'] = $result->created_at;
            foreach ($datas as $data) {
                if ($result->symbol == $data['symbol']) {
                    $company['sectorname'] = $data['sectorname'];
                    $company['sectorid'] = $data['sectorid'];
                    $company['id'] = $data['id'];
                    $company['companyname'] = $data['companyname'];
                }
            }

            array_push($price, $company);
        }
        return response()->json([
            'message' => 'Search result(s) found.',
            'type' => 'Success',
            'price' => $price
        ], 200);

    }
}
