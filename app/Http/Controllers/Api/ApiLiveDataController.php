<?php
/**
 * Created by PhpStorm.
 * User: rajendra
 * Date: 11/20/18
 * Time: 3:44 PM
 */

namespace App\Http\Controllers\api;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiLiveDataController extends Controller
{

    public function todayTradedStocks(Request $request)
    {
        $query = DB::table('tbl_live_floorsheet')
            ->selectRaw("
                contract_no, symbol, buyer, seller, quantity, rate, amount")
            ->groupBy('contract_no');
        $results = DB::table(DB::raw("(" . $query->toSql() . ") as flrsheet"))
            ->mergeBindings($query)
            ->selectRaw("
                symbol,
                COUNT(*) as no_of_trades,
                ROUND(SUBSTRING_INDEX(GROUP_CONCAT(IF(quantity >= 10, rate, NULL) ORDER BY contract_no ASC), ',', 1), 2) as open,
                ROUND(SUBSTRING_INDEX(GROUP_CONCAT(IF(quantity >= 10, rate, NULL) ORDER BY contract_no DESC), ',', 1), 2) as ltp,
                ROUND(SUM(amount)/SUM(quantity),2) as avg,
                ROUND(MAX(IF(quantity >= 10, rate, NULL)), 2) as high,
                ROUND(MIN(IF(quantity >= 10, rate, NULL)), 2) as low,
                ROUND(SUBSTRING_INDEX(GROUP_CONCAT(IF(quantity >= 10, rate, NULL) ORDER BY contract_no DESC), ',', 1), 2) as close,
                ROUND(SUM(quantity), 2) as traded_quantity, 
                ROUND(SUM(amount), 2) as traded_amount")
            ->groupBy('symbol')
            ->orderBy('symbol', 'ASC')
            ->get();
        return response()->json([
            'message' => 'Search result(s) found.',
            'type' => 'Success',
            'results' => $results
        ], 200);
    }

    public function todayTradedIndices(Request $request)
    {
        $results = DB::table('tbl_live_index')
            ->selectRaw("
                _index as indices,
                ROUND(SUBSTRING_INDEX(GROUP_CONCAT(CAST(current AS CHAR) ORDER BY created_at ASC), ',', 1), 2) as open,
                ROUND(SUBSTRING_INDEX(GROUP_CONCAT(CAST(current AS CHAR) ORDER BY created_at DESC), ',', 1), 2) as close,
                ROUND(SUBSTRING_INDEX(GROUP_CONCAT(CAST(turnover AS CHAR) ORDER BY created_at DESC), ',', 1), 2) as turnover,
                ROUND(MAX(current), 2) as high,
                ROUND(MIN(current), 2) as low")
            ->whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d') = '" . $request->input('date') . "'")
            ->groupBy('_index')
            ->get();
        return response()->json([
            'message' => 'Search result(s) found.',
            'type' => 'Success',
            'results' => $results
        ], 200);
    }

    public function todayFloorsheet(Request $request)
    {
        $results = DB::table('tbl_live_floorsheet')
            ->selectRaw("
                contract_no, symbol, buyer, seller, quantity, rate, amount")
            ->groupBy('contract_no')
            ->orderBy('contract_no', "ASC")
            ->get();
        return response()->json([
            'message' => 'Search result(s) found.',
            'type' => 'Success',
            'results' => $results
        ], 200);
    }
}