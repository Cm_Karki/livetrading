<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ApiFloorsheetController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function liveTradeAnalysis(Request $request)
    {
        if (!$request->has('symbol'))
            return response()->json([
                'message' => 'Bad request.',
                'type' => 'Error'
            ], 404);
        /**
         * Get price vs volume and no of transaction
         */
        $priceVsVlmAndTrnx = DB::table('tbl_live_floorsheet')
            ->selectRaw('rate, COUNT(*) as noOfTrnx, SUM(quantity) as volume')
            ->where('symbol', '=', $request->get('symbol'))
            ->groupBy('rate')
            ->orderBy('rate', 'ASC')
            ->get();
        /**
         * Get Top 5 buyers broker
         */
        $buyers = DB::table('tbl_live_floorsheet')
            ->selectRaw('CAST(buyer as CHAR(100)) as number, COALESCE(SUM(quantity), 0) as quantity,
                COALESCE(SUM(amount), 0) as amount, COALESCE(AVG(rate), 0) as rate')
            ->where('symbol', '=', $request->get('symbol'))
            ->groupBy('buyer')
            ->orderBy('amount', 'DESC')
            ->get();

        /**
         * Get Top 5 seller broker
         */
        $sellers = DB::table('tbl_live_floorsheet')
            ->selectRaw('CAST(seller as CHAR(100)) as number, COALESCE(SUM(quantity), 0) as quantity,
                COALESCE(SUM(amount), 0) as amount, COALESCE(AVG(rate), 0) as rate')
            ->where('symbol', '=', $request->get('symbol'))
            ->groupBy('seller')
            ->orderBy('amount', 'DESC')
            ->get();
        /**
         * Get today share price
         */
        $companies = DB::table('tbl_live_floorsheet')
            ->select(DB::raw('COALESCE(SUBSTRING_INDEX(GROUP_CONCAT(CAST(rate AS CHAR) ORDER BY contract_no DESC), ",", 1), 0) as ltp,
                            COALESCE(SUBSTRING_INDEX(GROUP_CONCAT(CAST(rate AS CHAR) ORDER BY contract_no ASC), ",", 1), 0) as open,
                            MAX(rate) as high, MIN(rate) as low, SUM(quantity) as volume, COUNT(*) as no_trade, AVG(rate) as weighted_avg_price'))
            ->where('symbol', '=', $request->get('symbol'))
            ->groupBy('symbol')
            ->get();

        return response()->json([
            'message' => 'Search result(s) found.',
            'type' => 'Success',
            'priceVsVlmAndTrnx' => $priceVsVlmAndTrnx,
            'buyers' => $buyers,
            'sellers' => $sellers,
            'companies' => $companies
        ], 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function liveBrokerAnalysis(Request $request)
    {
        /**
         * Get Top 5 buyers broker
         */
        $buyers = DB::table('tbl_live_floorsheet')
            ->selectRaw('CAST(buyer as CHAR(100)) as number, COALESCE(SUM(quantity), 0) as quantity,
                COALESCE(SUM(amount), 0) as amount')
            ->groupBy('buyer')
            ->orderBy('amount', 'DESC')
            ->get();

        /**
         * Get Top 5 seller broker
         */
        $sellers = DB::table('tbl_live_floorsheet')
            ->selectRaw('CAST(seller as CHAR(100)) as number, COALESCE(SUM(quantity), 0) as quantity,
                COALESCE(SUM(amount), 0) as amount')
            ->groupBy('seller')
            ->orderBy('amount', 'DESC')
            ->get();

        return response()->json([
            'message' => 'Search result(s) found.',
            'type' => 'Success',
            'buyers' => $buyers,
            'sellers' => $sellers
        ], 200);
    }

}
