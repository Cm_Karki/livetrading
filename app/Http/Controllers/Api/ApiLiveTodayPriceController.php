<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Api\ApiLiveTodayPrice;

class ApiLiveTodayPriceController extends Controller
{

    public function index(){
        $liveprice = ApiLiveTodayPrice::GetLiveStockPrice();
        return response()->json($liveprice, 200);
    }

    public function nepseIndex(){
        $nepseindex = ApiLiveTodayPrice::GetNepseIndexPrice();
        return response()->json($nepseindex, 200);
    }
    public function nepse(){
        $nepse = ApiLiveTodayPrice::getNepse();
        return response()->json($nepse, 200);
    }

    public function gainerlosser(){
        $total = ApiLiveTodayPrice::getTotalGainerLosser();
        return response()->json($total, 200);
    }
    
    public function todayNepse(){
        $nepse = ApiLiveTodayPrice::getAllTodayNepse();
        return response()->json($nepse, 200);
    }

    public function liveData(Request $request){
        $livedata = ApiLiveTodayPrice::getAllLiveData($request->from,$request->to,$request->symbol,$request->type, $request->resolution);
        return response()->json($livedata, 200);
    }
    public function marketStat(){
        $market = ApiLiveTodayPrice::getAllmarketStat();
        return response()->json($market, 200);
    }
    
    public function stockVolume(Request $request){
        $market = ApiLiveTodayPrice::getTotalVolume($request->symbol);
        return response()->json($market, 200);
    }
    
}
