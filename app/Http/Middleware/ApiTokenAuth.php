<?php

namespace App\Http\Middleware;

use Closure;

class ApiTokenAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('API-KEY');
        if ('SS_LIVETRADING_$!@77&' != $token) {
            abort(401, 'Unauthorized user');
        }
        return $next($request);
    }
}
