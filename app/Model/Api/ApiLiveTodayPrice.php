<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ApiLiveTodayPrice extends Model
{
    protected $table = 'tbl_live_stock';
    protected $guarded = ['id'];

    public static function GetLiveStockPrice(){
            $latestdate=DB::table('tbl_live_stock')
                        ->select('created_at')
                        ->orderBy('created_at','DESC')
                        ->first();
            $data = DB::table('tbl_live_stock')
                    ->select(DB::raw('symbol, ltp, ltv,change_, open, high, low, 
                        CASE WHEN change_ < 0 THEN per_change*-1  ELSE per_change END as per_change, traded_quantity, prev_close,created_at '))
                    ->where('created_at',$latestdate->created_at)
                    ->get();
            return $data;
    }
    public static function GetNepseIndexPrice(){
        $latestdate=DB::table('tbl_live_index')
            ->select('created_at')
            ->orderBy('created_at','DESC')
            ->first();
        $data = DB::table('tbl_live_index')
            ->select(DB::raw("
                SUBSTRING_INDEX(GROUP_CONCAT(CAST(_index AS CHAR) ORDER BY created_at DESC), ',', 1 ) as _index,
                SUBSTRING_INDEX(GROUP_CONCAT(CAST(turnover AS CHAR) ORDER BY created_at DESC), ',', 1 ) as turnover,
                SUBSTRING_INDEX(GROUP_CONCAT(CAST(current AS CHAR) ORDER BY created_at DESC), ',', 1 ) as current,
                SUBSTRING_INDEX(GROUP_CONCAT(CAST(ABS(change_) AS CHAR) ORDER BY created_at DESC), ',', 1 ) as change_,
                SUBSTRING_INDEX(GROUP_CONCAT(CAST(ABS(per_change) AS CHAR) ORDER BY created_at DESC), ',', 1 ) as per_change,
                SUBSTRING_INDEX(GROUP_CONCAT(CAST(inc_dec AS CHAR) ORDER BY created_at DESC), ',', 1 ) as inc_dec, '" .
                $latestdate->created_at ."' as created_at"))
            ->where('created_at', '>', date('Y-m-d', strtotime($latestdate->created_at)))
            ->groupBy('_index')
            ->get();
        return $data;
    }

    public static function getAllLiveData($from,$to,$symbol,$type, $resolution){
        if ($type == "INDEX") {
            DB::statement(DB::raw("SET @days := 0"));
            DB::statement(DB::raw("SET @prevCurr := 0.00"));
            DB::statement(DB::raw("SET @prevTrn := 0"));

            $query1 = DB::table('tbl_live_index')
                ->select(DB::raw("created_at, current, @prevCurr as prev_current,
                    (@prevCurr:=current), turnover"))
                ->where('_index', $symbol)
                ->orderBy('created_at', 'ASC');
            $query = DB::table(DB::raw("(" . $query1->toSql() . ") as query1"))
                ->mergeBindings($query1)
                ->select(DB::raw("created_at as date,
                    IF(@days = DATE_FORMAT(created_at, '%d'), @prevCurr:=prev_current, @prevCurr:=current) as open,
                    IF(current > @prevCurr, current, @prevCurr) as high,
                    IF(current < @prevCurr, current, @prevCurr) as low,
                    current as close,
                    IF(@days = DATE_FORMAT(created_at, '%d'), @prevTrn:=@prevTrn, @prevTrn:=0),
                    (turnover - @prevTrn) as volume,
                    (@prevTrn:=turnover),
                    (@days:=DATE_FORMAT(created_at, '%d')),
                    turnover"))
                ->whereBetween('created_at', array(date('Y-m-d h:i:s', $from), date('Y-m-d h:i:s', $to + 86400)))
                ->orderBy('created_at', 'ASC');
            if (strpos($resolution, 'D') !== false) {
                return DB::table(DB::raw("(" . $query->toSql() . ") as query"))
                    ->mergeBindings($query)
                    ->select(DB::raw("DATE_FORMAT(date, '%Y-%m-%d') as date,
                         SUBSTRING_INDEX(MIN(CONCAT(UNIX_TIMESTAMP(date), '_', open)), '_', -1) AS open,
                         MIN(low) as low, MAX(high) as high,
                         SUBSTRING_INDEX(MAX(CONCAT(UNIX_TIMESTAMP(date), '_', close)), '_', -1) AS close,
                         SUM(volume) as volume"))
                    ->groupBy(DB::raw("DATE_FORMAT(date, '%Y-%m-%d')"))
                    ->orderBy('date', 'ASC')
                    ->get();
            } else {
                return $query->get();
            }
        }
        if ($type == "STOCK") {
            DB::statement(DB::raw("SET @days := 0"));
            DB::statement(DB::raw("SET @prevLtp := 0.00"));
            DB::statement(DB::raw("SET @prevTrdQty := 0"));

            $query1 = DB::table('tbl_live_stock')
                ->select(DB::raw("created_at, ltp, @prevLtp as prev_ltp,
                    (@prevLtp:=ltp), traded_quantity"))
                ->where('symbol', $symbol)
                ->orderBy('created_at', 'ASC');

            $query = DB::table(DB::raw("(" . $query1->toSql() . ") as query1"))
                ->mergeBindings($query1)
                ->select(DB::raw("created_at as date,
                    IF(@days = DATE_FORMAT(created_at, '%d'), @prevLtp:=prev_ltp, @prevLtp:=ltp) as open,
                    IF(ltp > @prevLtp, ltp, @prevLtp) as high,
                    IF(ltp < @prevLtp, ltp, @prevLtp) as low,
                    ltp as close,
                    IF(@days = DATE_FORMAT(created_at, '%d'), @prevTrdQty:=@prevTrdQty, @prevTrdQty:=0),
                    (traded_quantity - @prevTrdQty) as volume,
                    (@prevTrdQty:=traded_quantity),
                    (@days:=DATE_FORMAT(created_at, '%d')),
                    traded_quantity"))
                ->whereBetween('created_at', array(date('Y-m-d h:i:s', $from), date('Y-m-d h:i:s', $to + 86400)))
                ->orderBy('created_at', 'ASC');

            if (strpos($resolution, 'D') !== false) {
                return DB::table(DB::raw("(" . $query->toSql() . ") as query"))
                    ->mergeBindings($query)
                    ->select(DB::raw("DATE_FORMAT(date, '%Y-%m-%d') as date,
                         SUBSTRING_INDEX(MIN(CONCAT(UNIX_TIMESTAMP(date), '_', open)), '_', -1) AS open,
                         MIN(low) as low, MAX(high) as high,
                         SUBSTRING_INDEX(MAX(CONCAT(UNIX_TIMESTAMP(date), '_', close)), '_', -1) AS close,
                         SUM(volume) as volume"))
                    ->groupBy(DB::raw("DATE_FORMAT(date, '%Y-%m-%d')"))
                    ->orderBy('date', 'ASC')
                    ->get();
            } else {
                return $query->get();
            }
        }

    }
    public static function getAllTodayNepse(){
        $latestdate=DB::table('tbl_live_index')
                ->select('created_at')
                ->orderBy('created_at','DESC')
                ->first();
        $date = date("Y-m-d",strtotime($latestdate->created_at));
        $data = DB::table('tbl_live_index')
                ->select(DB::raw('DATE_FORMAT(created_at, "%H:%i") as time,current,inc_dec,_index,created_at'))
                ->where('created_at','>=',$date)
                ->where('_index','NEPSE')
                ->orderBy('created_at','ASC')
                ->get();
        $previous = DB::table('tbl_live_index')
                 ->select('current','inc_dec','_index','created_at')
                 ->where('created_at','<',$date)
                 ->where('_index','NEPSE')
                 ->orderBy('created_at','DESC')
                 ->first();
        return [
                'previous' => $previous,
                'current' => $data
        ];
    }

    public static function getNepse(){
        
        $data = DB::table('tbl_live_index')
                ->where('_index','NEPSE')
                ->orderBy('created_at','DESC')
                ->first();
        return $data;
}

        public static function getAllmarketStat(){
                $date = DB::table('tbl_live_market_summary')
                        ->select('created_at')
                        ->orderBy('created_at','DESC')
                        ->first();
                $date1 = DB::table('tbl_live_stock')
                        ->select('created_at')
                        ->orderBy('created_at','DESC')
                        ->first();
                $data = DB::table('tbl_live_market_summary')
                        ->where('created_at',$date->created_at)
                        ->get();
                
                $data2 = DB::table('tbl_live_stock')
                        ->select(DB::raw(' sum(case when change_ >0 then 1 else 0 end) as gainer, 
                        sum(case when change_ < 0 then 1 else 0 end) as losser, 
                        sum(case when change_ = 0 then 1 else 0 end) as nochange'))
                        ->where('created_at',$date1->created_at)
                        ->first();
                $value = $data2->gainer . "/". $data2->losser ."/". $data2->nochange;
                return [
                        'gainer' => $value,
                        'market' => $data
                ];
        }
    public static function getTotalGainerLosser(){
        $datas = array();

        $latestdate=DB::table('tbl_live_stock')
                        ->select('created_at')
                        ->orderBy('created_at','DESC')
                        ->first();

        $query1 = DB::table('tbl_live_stock')
                    ->where('created_at',$latestdate->created_at)
                    ->where('change_','>',0)
                    ->orderBy('per_change','DESC')
                    ->limit(10);

        $gain = DB::table(DB::raw('(' . $query1->toSql() . ') sub'))
            ->mergeBindings($query1)
            ->sum('per_change');
        array_push($datas, $gain);
                
        $query2 = DB::table('tbl_live_stock')
        ->where('created_at',$latestdate->created_at)
        ->where('change_','<',0)
        ->orderBy('per_change','DESC')
        ->limit(10);

         $loss = DB::table(DB::raw('(' . $query2->toSql() . ') sub'))
            ->mergeBindings($query2)
            ->sum('per_change');
            array_push($datas, $loss);

            return $datas;
    }

    public static function getTotalVolume($symbol){
        $date = $latestdate=DB::table('tbl_live_stock')
                ->select('created_at')
                ->orderBy('created_at','DESC')
                ->first();

        $newdate = date("d-m-Y", strtotime($date->created_at));
        $result =  DB::table('tbl_live_stock')
                ->select('id','symbol',DB::raw('SUM(ltv) as ltv'))
                ->where('symbol',$symbol)
                ->whereDate('created_at','>=',$newdate)
                ->groupBy('ltp')
                ->get();
        
        return $result;

    }
}
